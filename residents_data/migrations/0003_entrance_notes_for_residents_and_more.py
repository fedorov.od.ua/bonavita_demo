# Generated by Django 5.0.6 on 2024-05-26 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0002_alter_entrance_options_alter_floor_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='entrance',
            name='notes_for_residents',
            field=models.TextField(max_length=255, null=True, verbose_name='Інші примітки'),
        ),
        migrations.AddField(
            model_name='entrance',
            name='notes_for_stikon',
            field=models.TextField(max_length=255, null=True, verbose_name='Примітки по Стікону'),
        ),
        migrations.AlterField(
            model_name='floor',
            name='building_number',
            field=models.CharField(max_length=63, null=True, verbose_name='Будівельний номер'),
        ),
        migrations.AlterField(
            model_name='floor',
            name='number',
            field=models.IntegerField(unique=True, verbose_name='Поштовий номер'),
        ),
    ]
