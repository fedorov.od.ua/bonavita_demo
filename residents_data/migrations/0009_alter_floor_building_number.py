# Generated by Django 5.0.6 on 2024-05-26 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0008_floor_building_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='floor',
            name='building_number',
            field=models.IntegerField(null=True, verbose_name='Будівельний номер'),
        ),
    ]
