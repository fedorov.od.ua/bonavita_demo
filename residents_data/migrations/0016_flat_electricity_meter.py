# Generated by Django 5.0.6 on 2024-05-27 19:02

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0015_electricitymeter_alter_flat_notes_for_residents_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='flat',
            name='electricity_meter',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='el_meter', to='residents_data.electricitymeter'),
        ),
    ]
