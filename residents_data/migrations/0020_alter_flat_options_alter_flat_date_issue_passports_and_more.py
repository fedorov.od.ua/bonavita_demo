# Generated by Django 5.0.6 on 2024-05-28 17:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0019_alter_flat_floor'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flat',
            options={'ordering': ('entrance', 'number'), 'verbose_name': 'Квартира', 'verbose_name_plural': 'Квартири'},
        ),
        migrations.AlterField(
            model_name='flat',
            name='date_issue_passports',
            field=models.DateField(blank=True, null=True, verbose_name='Дата видачі'),
        ),
        migrations.AlterField(
            model_name='flat',
            name='owner',
            field=models.CharField(max_length=63, null=True, verbose_name='Власник'),
        ),
    ]
