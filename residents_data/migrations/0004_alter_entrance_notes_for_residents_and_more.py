# Generated by Django 5.0.6 on 2024-05-26 15:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0003_entrance_notes_for_residents_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entrance',
            name='notes_for_residents',
            field=models.TextField(default=False, max_length=255, null=True, verbose_name='Інші примітки'),
        ),
        migrations.AlterField(
            model_name='entrance',
            name='notes_for_stikon',
            field=models.TextField(default=False, max_length=255, null=True, verbose_name='Примітки по Стікону'),
        ),
        migrations.AlterField(
            model_name='entrance',
            name='number',
            field=models.CharField(max_length=63, unique=True, verbose_name="Номер під'їзду"),
        ),
    ]
