# Generated by Django 5.0.6 on 2024-05-26 23:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0013_alter_flat_building_number_alter_flat_fire_detector_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flat',
            options={'verbose_name': 'Квартира', 'verbose_name_plural': 'Квартири'},
        ),
        migrations.AddField(
            model_name='flat',
            name='date_issue_passports',
            field=models.DateField(null=True, verbose_name='Дата видачі'),
        ),
        migrations.AddField(
            model_name='flat',
            name='debtor_status',
            field=models.BooleanField(default=False, verbose_name='Боржник'),
        ),
        migrations.AddField(
            model_name='flat',
            name='notes_for_residents',
            field=models.TextField(max_length=255, null=True, verbose_name='Інші примітки'),
        ),
        migrations.AddField(
            model_name='flat',
            name='passports_for_meters',
            field=models.BooleanField(default=False, verbose_name='Паспорта на счетчики'),
        ),
        migrations.AddField(
            model_name='flat',
            name='personal_account',
            field=models.IntegerField(null=True, verbose_name='Номер л/с'),
        ),
        migrations.AddField(
            model_name='flat',
            name='removing_date_for_fire_det',
            field=models.DateField(null=True, verbose_name='Дата зняття'),
        ),
        migrations.AddField(
            model_name='flat',
            name='set_up_date_for_fire_det',
            field=models.DateField(null=True, verbose_name='Дата встановлення'),
        ),
        migrations.AddField(
            model_name='flat',
            name='shutdown_date',
            field=models.DateField(null=True, verbose_name='Дата відключення'),
        ),
        migrations.AlterField(
            model_name='flat',
            name='radiator',
            field=models.IntegerField(null=True, verbose_name='Радіатори'),
        ),
    ]
