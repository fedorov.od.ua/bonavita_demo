# Generated by Django 5.0.6 on 2024-05-29 14:03

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('residents_data', '0028_remove_meter_electricity_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meter',
            name='flat',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='meters', to='residents_data.flat'),
        ),
    ]
