from django.db import models
from django.utils import timezone

FLOORS = [(-1, -1)] + [(x, x) for x in range(1, 17)]


class Flat(models.Model):
    class Status(models.TextChoices):
        RENOVATION = 'Renov', 'Ремонт'
        RESIDENTIAL = 'Resid', 'Житлова'

    number = models.CharField(max_length=5, verbose_name="Пошт. номер")
    entrance = models.CharField(blank=False, null=True, max_length=3, verbose_name="Під'їзд")
    floor = models.IntegerField(null=True, choices=FLOORS, verbose_name="Поверх")
    building_number = models.IntegerField(blank=False, null=True, verbose_name="Буд. номер")
    owner = models.CharField(max_length=63, null=True, blank=True, verbose_name="П.І.Б. власника")
    radiator = models.IntegerField(null=True, blank=True, verbose_name="Радіатори")
    status = models.CharField(max_length=5, choices=Status.choices, default=Status.RENOVATION, verbose_name="Статус")
    electricity_number = models.IntegerField(blank=True, null=True, verbose_name="Ел/лічильник №")
    water_number = models.IntegerField(blank=True, null=True, verbose_name="Водомір №")
    heat_number = models.IntegerField(blank=True, null=True, verbose_name="Теплолічильник №")
    fire_detector = models.BooleanField(default=False, verbose_name="Пож. датчик")
    removing_date_for_fire_det = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True, verbose_name="Дата зняття")
    set_up_date_for_fire_det = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True, verbose_name="Дата встановлення")
    automat10 = models.BooleanField(default=False, verbose_name="Автомат 10")
    removing_date_for_automat10 = models.DateField(auto_now=False, auto_now_add=False, null=True,
                                                   blank=True, verbose_name="Дата зняття")
    set_up_date_for_automat10 = models.DateField(auto_now=False, auto_now_add=False, null=True,
                                                 blank=True, verbose_name="Дата встановлення")
    automat50 = models.BooleanField(default=False, verbose_name="Автомат 50")
    appartment_notes = models.TextField(max_length=63, null=True, blank=True, verbose_name="Примітки")
    passports_for_meters = models.BooleanField(default=False, verbose_name="Паспорта на лічильники")
    date_issue_passports = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True, verbose_name="Дата видачі")
    personal_account = models.IntegerField(null=True, blank=True, verbose_name="Номер л/р")
    debtor_status = models.BooleanField(default=False, verbose_name="Боржник")
    shutdown_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True, verbose_name="Дата відключення")

    class Meta:
        verbose_name = "Квартира"
        verbose_name_plural = "Квартири"
        ordering = ("entrance", "number")

    def __str__(self):
        return f"Квартира {self.number}, під'їзд {self.entrance}, поверх {self.floor}"


class Meter(models.Model):
    electro_value = models.IntegerField(blank=True, null=True, verbose_name="Показання ел/лічильника")
    water_value = models.IntegerField(blank=True, null=True, verbose_name="Показання водоміру")
    heat_value = models.FloatField(blank=True, null=True, verbose_name="Показання теплолічильника")
    date_reading = models.DateField(auto_now=False, auto_now_add=False, null=True,
                                                   verbose_name="Дата зняття показань")
    flat = models.ForeignKey("Flat", on_delete=models.SET_NULL, null=True, blank=True, related_name="meters")

    class Meta:
        verbose_name = "Показання лічильника"
        verbose_name_plural = "Показання лічильників"

    def __str__(self):
        return str(self.date_reading)


class Note(models.Model):
    class IssueType(models.TextChoices):
        note = "N", "Примітка"
        task = "T", "Завдання"

    status = models.BooleanField(default=False, verbose_name="Статус")
    issue_type = models.CharField(max_length=5, choices=IssueType.choices, verbose_name="Тип примітки")
    time_stamp = models.DateTimeField(auto_now_add=True, verbose_name="Дата створення")
    text = models.TextField(verbose_name="Опис")
    completion_date = models.DateTimeField(auto_now_add=False, auto_now=False, verbose_name="Термін дії")
    entrance = models.CharField(max_length=3, null=True, blank=True, verbose_name="Під'їзд")
    floor = models.IntegerField(choices=FLOORS, verbose_name="Поверх")
    flat = models.ForeignKey("Flat", on_delete=models.CASCADE, null=True, blank=True, related_name="notes")

    @property
    def completion_status(self):
        if timezone.now() > self.completion_date and self.status is False:
            return "Прострочено"
        elif self.status is True:
            return "Виконано"

    class Meta:
        verbose_name = "Завдання та примітки"
        verbose_name_plural = "Завдання та примітки"
        ordering = ("time_stamp",)

    def __str__(self):
        if self.issue_type == self.IssueType.note:
            return "Примітка"
        else:
            return "Завдання"
