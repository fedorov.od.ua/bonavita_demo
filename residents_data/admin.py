from django.contrib import admin
from django.utils import timezone

from residents_data import models
from residents_data.models import Meter, Note
from django_admin_inline_paginator.admin import TabularInlinePaginated


class MeterInline(TabularInlinePaginated):
    model = Meter
    # can_delete = False
    extra = 0
    ordering = ("-date_reading", )
    # classes = ("collapse", )
    per_page = 1


@admin.register(models.Flat)
class FlatAdmin(admin.ModelAdmin):
    list_display = ("number", "building_number", "owner", "status", "debtor_status", "floor", "entrance")
    list_filter = ("entrance", "status", )
    search_fields = ["owner"]
    fieldsets = (
        ("Загальна інфо", {"fields": ((
            "number", "building_number"), ("entrance", "floor"), ("status", ))}
         ),
        ("Інфо власника", {"fields": ((
            "owner", "personal_account"), ), "classes": ("collapse", )}
         ),
        ("Пристрої", {"fields": (
            ("electricity_number", "water_number", "heat_number"),
            ("fire_detector", "removing_date_for_fire_det", "set_up_date_for_fire_det"),
            ("automat10", "removing_date_for_automat10", "set_up_date_for_automat10"),
            ("automat50", ),
            ("radiator", )), "classes": ("collapse", )}
         ),
        ("Додаткова інформація", {"fields": (
            ("passports_for_meters", "date_issue_passports"),
            ("debtor_status", "shutdown_date"),
            "appartment_notes"), "classes": ("collapse", )}
         ),
    )
    inlines = [MeterInline]


@admin.register(models.Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ("time_stamp", "issue_type", "completion_date",
                    "completion_status", "entrance", "floor", "flat")
    list_filter = ("status", "entrance", "issue_type")
    list_per_page = 10
