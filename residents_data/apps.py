from django.apps import AppConfig


class ResidentsDataConfig(AppConfig):
    verbose_name = "Інформація по об'єкту"
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'residents_data'
